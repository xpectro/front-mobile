import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
} from "react-native";
import css from "../constants/css";
import * as Auth from "../middleware/auth";

export default function RecetasScreen() {
  const [modalVisible, setModalVisible] = useState(false);
  const [text, setText] = useState("breakfast");
  const [isLoading, setLoading] = useState(true);
  const [datos, setDatos] = useState([]);

  useEffect(() => {
    async function getDoc() {
      const doc = await Auth.default._retrieveData();
      console.log(doc);
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          user_document: doc,
        },
      };
      //console.log(doc);
      fetch("http://192.168.1.7:3050/recipes/recipe_movil/" + doc, datasend)
        .then((response) => response.json())
        .then((json) => setDatos(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }
    getDoc();
  }, []);

  return (
    <View style={css.cc}>
      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <View style={css.centeredView}>
          <View style={css.modalView}>
            <View>
              {text === "breakfast" ? (
                <FlatList
                  key="id"
                  data={datos}
                  renderItem={({ item }) => (
                    <Item
                      key="idp"
                      title={item.breakfast}
                      title1={item.breakfast2}
                      title2={item.breakfast3}
                    />
                  )}
                  keyExtractor={(item) => item.breakfast}
                />
              ) : text === "midmorning" ? (
                <FlatList
                  key="id"
                  data={datos}
                  renderItem={({ item }) => (
                    <Item
                      key="idp"
                      title={item.midmorning}
                      title1={item.midmorning2}
                      title2={item.midmorning3}
                    />
                  )}
                  keyExtractor={(item) => item.breakfast}
                />
              ) : text === "lunch" ? (
                <FlatList
                  key="id"
                  data={datos}
                  renderItem={({ item }) => (
                    <Item
                      key="idp"
                      title={item.lunch}
                      title1={item.lunch2}
                      title2={item.lunch3}
                    />
                  )}
                  keyExtractor={(item) => item.breakfast}
                />
              ) : text === "midafternoon" ? (
                <FlatList
                  key="id"
                  data={datos}
                  renderItem={({ item }) => (
                    <Item
                      key="idp"
                      title={item.midafternoon}
                      title1={item.midafternoon2}
                      title2={item.midafternoon3}
                    />
                  )}
                  keyExtractor={(item) => item.breakfast}
                />
              ) : text === "dinner" ? (
                <FlatList
                  key="id"
                  data={datos}
                  renderItem={({ item }) => (
                    <Item
                      key="idp"
                      title={item.dinner}
                      title1={item.dinner2}
                      title2={item.dinner3}
                    />
                  )}
                  keyExtractor={(item) => item.breakfast}
                />
              ) : null}

              <TouchableHighlight
                style={{ ...css.openButton, backgroundColor: "red", top: 10 }}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <View>
                  <Text style={css.textStyle}>Cerrar</Text>
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>

      <View style={css.titulo_container}>
        <Text style={css.titulo}>Mis Recetas</Text>
      </View>

      <View style={css.press}>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(!modalVisible);
            setText("breakfast");
          }}
        >
          <Text style={css.subtitulo}>Breakfast</Text>
          <Image
            source={require("../assets/images/breakfast.jpg")}
            style={css.image}
          />
        </TouchableOpacity>
      </View>
      <View style={css.press}>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(!modalVisible);
            setText("midmorning");
          }}
        >
          <Text style={css.subtitulo}>MidMorning</Text>
          <Image
            source={require("../assets/images/midmorning.jpg")}
            style={css.image}
          />
        </TouchableOpacity>
      </View>
      <View style={css.press}>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(!modalVisible);
            setText("lunch");
          }}
        >
          <Text style={css.subtitulo}>Lunch</Text>
          <Image
            source={require("../assets/images/lunch.jpg")}
            style={css.image}
          />
        </TouchableOpacity>
      </View>
      <View style={css.press}>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(!modalVisible);
            setText("midafternoon");
          }}
        >
          <Text style={css.subtitulo}>MidAfternoon</Text>
          <Image
            source={require("../assets/images/midafternoon.jpg")}
            style={css.image}
          />
        </TouchableOpacity>
      </View>
      <View style={css.press}>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(!modalVisible);
            setText("dinner");
          }}
        >
          <Text style={css.subtitulo}>Dinner</Text>
          <Image
            source={require("../assets/images/dinner.jpg")}
            style={css.image}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

function Item({ title, title1, title2 }) {
  return (
    <View>
      <Text
        style={{
          marginBottom: 25,
          borderBottomWidth: 1,
          borderBottomColor: "gray",
        }}
      >
        {title}
      </Text>
      <Text
        style={{
          marginBottom: 25,
          borderBottomWidth: 1,
          borderBottomColor: "gray",
        }}
      >
        {title1}
      </Text>
      <Text style={{ borderBottomWidth: 1, borderBottomColor: "gray" }}>
        {title2}
      </Text>
    </View>
  );
}
