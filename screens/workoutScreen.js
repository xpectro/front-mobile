import { Ionicons } from "@expo/vector-icons";
import Icon from "react-native-vector-icons/FontAwesome";
import * as WebBrowser from "expo-web-browser";
import css from "../constants/css";
import {
  StyleSheet,
  Text,
  Modal,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { RectButton, ScrollView } from "react-native-gesture-handler";
import * as Auth from "../middleware/auth";
import React, { useState, useEffect } from "react";

export default function workoutScreen({ navigation }) {
  const [name, setName] = useState("");
  const [datos, setDatos] = useState([]);
  const [machines, setMachines] = useState(null);
  const [photo, setPhoto] = useState("");
  const [isLoading, setLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    async function getDoc() {
      const doc = await Auth.default._retrieveData();
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          user_document: doc,
        },
      };
      fetch("http://192.168.1.7:3050/plan_machines/workout/" + doc, datasend)
        .then((response) => response.json())
        .then((json) => setDatos(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }
    getDoc();
  }, []);

  /**************************************************** */
  /**************************************************** */

  let info = [];
  const save = (text) => {
    setName(text);
    datos.forEach((element) => {
      let req = element["machine_type"];
      if (req === text) {
        let js = {
          machine_name: element["machine_name"],
          machine_photo: element["machine_photo"],
          machine_serial: element["machine_serial"],
        };
        info.push(js);
      }
    });
    setMachines(info);
  };

  /**************************************************** */
  /**************************************************** */
  const combined = (photo) => {
    setModalVisible(true);
    setPhoto(photo);
  };
  /**************************************************** */
  /**************************************************** */

  const Item = ({ title, photo }) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => combined(photo)}
          style={{ backgroundColor: "#DEF0FF", marginBottom: 15 }}
        >
          <Text
            style={{
              fontSize: 20,
            }}
          >
            <Icon name="star" size={16} color="#00497C" />
            {title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={css.contenedor_workout}>
      <View style={css.titulo_container}>
        <Text style={css.titulo_plan}>Mi Plan</Text>
      </View>
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={css.centeredView}>
          <View style={css.modalView}>
            {photo === "banco_plano.gif" ? (
              <Image
                source={require("../assets/images/banco_plano.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "banco_declinado.gif" ? (
              <Image
                source={require("../assets/images/banco_declinado.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "banco_inclinado.gif" ? (
              <Image
                source={require("../assets/images/banco_inclinado.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "biceps.gif" ? (
              <Image
                source={require("../assets/images/biceps.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "fondos.gif" ? (
              <Image
                source={require("../assets/images/fondos.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "curl_barra.gif" ? (
              <Image
                source={require("../assets/images/biceps.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "curl_barra.gif" ? (
              <Image
                source={require("../assets/images/curl_barra.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "curl_mancuerna.gif" ? (
              <Image
                source={require("../assets/images/curl_mancuerna.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "curl_sentado.gif" ? (
              <Image
                source={require("../assets/images/curl_sentado.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "curl_sentado_multiple.gif" ? (
              <Image
                source={require("../assets/images/curl_sentado_multiple.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "triceps_superior.gif" ? (
              <Image
                source={require("../assets/images/triceps_superior.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "triceps_maquina.gif" ? (
              <Image
                source={require("../assets/images/triceps_maquina.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "dips_cable.gif" ? (
              <Image
                source={require("../assets/images/dips_cable.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "triceps_fondos.gif" ? (
              <Image
                source={require("../assets/images/triceps_fondos.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "remo_barra.gif" ? (
              <Image
                source={require("../assets/images/remo_barra.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "remo_punta.gif" ? (
              <Image
                source={require("../assets/images/remo_punta.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "extension_barra_adelante.gif" ? (
              <Image
                source={require("../assets/images/extension_barra_adelante.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "elevaciones_laterales.gif" ? (
              <Image
                source={require("../assets/images/elevaciones_laterales.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "extension_mancuernas.gif" ? (
              <Image
                source={require("../assets/images/extension_mancuernas.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "sentadilla_mancuernas.gif" ? (
              <Image
                source={require("../assets/images/sentadilla_mancuernas.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "step-up_mancuernas.gif" ? (
              <Image
                source={require("../assets/images/step-up_mancuernas.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "leg_press.gif" ? (
              <Image
                source={require("../assets/images/leg_press.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "leg_extension.gif" ? (
              <Image
                source={require("../assets/images/leg_extension.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "elevacion_bajada.gif" ? (
              <Image
                source={require("../assets/images/elevacion_bajada.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "abdominal_pelota_superior.gif" ? (
              <Image
                source={require("../assets/images/abdominal_pelota_superior.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "plancha.gif" ? (
              <Image
                source={require("../assets/images/plancha.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "levantado_pantorrilas.gif" ? (
              <Image
                source={require("../assets/images/levantado_pantorrilas.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : photo === "gemelo_sentado.gif" ? (
              <Image
                source={require("../assets/images/gemelo_sentado.gif")}
                style={{ top: "1%", width: "100%", height: "85%" }}
              />
            ) : null}
            <TouchableOpacity
              style={{ ...css.openButton, backgroundColor: "red", top: 45 }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={css.textStyle}>Cerrar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      {/**************************************************** */}
      {/**************************************************** */}

      <View style={css.contenedor_selectores}>
        <TouchableOpacity
          onPress={() => save(7)} //pecho
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Pecho</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(8)} //biceps
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Biceps</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(10)} //triceps
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Triceps</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(11)} //espalda
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Espalda</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(12)} //hombros
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Hombros</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(9)} //piernas
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Piernas</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(14)} //abdomen
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Abdomen</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => save(13)} //pantorrilla
          style={css.btn_ejercicios}
        >
          <Text style={css.text__musculos}>Pantorrillas</Text>
        </TouchableOpacity>
      </View>

      {/**************************************************** */}
      {/**************************************************** */}

      <View style={css.contenedor_imagenes_ejercicios}>
        {machines !== null ? (
          <FlatList
            key="id0"
            data={machines}
            renderItem={({ item }) => (
              <Item
                key="idp0"
                title={item.machine_name}
                photo={item.machine_photo}
              />
            )}
            keyExtractor={(item) => item.machine_serial}
          />
        ) : (
          <Image
            source={require("../assets/images/prueba.jpg")}
            style={{ top: "15%", width: "100%", height: "87%" }}
          />
        )}
      </View>
    </View>
  );
}
