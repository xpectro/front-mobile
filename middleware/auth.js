import { AsyncStorage } from "react-native";

const _storeData = async (document) => {
  try {
    await AsyncStorage.setItem("doc:key", document);
  } catch (error) {
    // Error saving data
  }
};

const _retrieveData = async () => {
  try {
    const value = await AsyncStorage.getItem("doc:key");
    if (value !== null) {
      // We have data!!
      return value;
      //console.log(value);
    }
  } catch (error) {
    console.log(error);
  }
};

export default { _storeData, _retrieveData };
