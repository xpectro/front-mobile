import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    //alignContent: "center",
    alignItems: "center",
    //backgroundColor:"#000"
  },
  input_container_document: {
    top: "40%",
    marginHorizontal: "10%",
  },
  input_container_password: {
    top: "50%",
    marginHorizontal: "10%",
  },
  input_login: {
    textAlign: "center",
  },
  btn_login: {
    top: "60%",
    //width: "40%",
    backgroundColor: "#00497C",
  },
  div_recetas: {
    top: 30,
    width: "100%",
    height: "100%",
  },
  image_container: {
    marginTop: 20,
    width: "95%",
  },
  titulo: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  subtitulo: {
    position: "relative",
    width: "100%",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
    backgroundColor: "#E4E2D1",
  },
  titulo_container: {
    position: "relative",
    top: "3.2%",
    height: "6%",
    width: "100%",
    backgroundColor: "#E4E2D1",
    marginBottom: "5%",
  },
  press: {
    position: "relative",
    top: "5%",
    //backgroundColor: "red",
    width: "90%",
    height: "15%",
    marginBottom: "3%",
    //borderWidth: 1,
  },
  cc: {
    height: "100%",
    alignItems: "center",
  },
  image: {
    position: "relative",
    //top: "6%",
    width: "100%",
    height: "80%",
  },
  //*********************************** */
  /*/MODAL*/
  //*********************************** */

  centeredView: {
    backgroundColor: "transparent",
    flex: 1,
    //justifyContent: "center",
    //alignItems: "center",
    marginTop: "10%",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    color: "black",
  },

  //************************************** */
  //  WorkOut
  //************************************** */

  contenedor_workout: {
    height: "100%",
    //alignItems: "center",
    //top: "3%",
  },
  selector: {
    position: "relative",
    top: "1%",
    width: "70%",
    height: "10%",
    borderWidth: 1,
    borderRadius: 100,
  },
  titulo_plan: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#E4E2D1",
  },
  btn_ejercicios: {
    position: "relative",
    width: "100%",
    backgroundColor: "#E4E2D1",
    marginBottom: "10%",
    borderRadius: 22,
  },
  text__musculos: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
  contenedor_imagenes_ejercicios: {
    position: "absolute",
    top: "11%",
    left: "35%",
    width: "65%",
    height: "100%",
    flexDirection: "row",
    alignItems: "flex-start",
    flexWrap: "wrap",
    //borderWidth: 1,
    //backgroundColor: "black",
    paddingHorizontal: "10%",
  },
  contenedor_selectores: {
    position: "absolute",
    top: "11%",
    width: "35%",
    height: "100%",
    flexDirection: "row",
    alignItems: "flex-start",
    flexWrap: "wrap",
    paddingLeft: "10%",
    //borderWidth: 1,
  },
});

export default styles;
