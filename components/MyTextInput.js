import React, { useState, useEffect } from "react";
import { Image, SafeAreaView, ToastAndroid } from "react-native";
import { Input, Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import css from "../constants/css";
import { useNavigation } from "@react-navigation/native";
import * as Auth from "../middleware/auth";

export default function MyTextInput() {
  const navigation = useNavigation();
  const [document, setDocument] = useState("");
  const [password, setPassword] = useState("");

  return (
    <SafeAreaView style={css.container}>
      <Image
        source={require("../assets/images/logo.png")}
        style={{ top: "15%", width: "80%", height: "30%" }}
      />
      <Input
        onChangeText={(document) => setDocument(document)}
        inputStyle={css.input_login}
        inputContainerStyle={css.input_container_document}
        title="button title"
        placeholder="Document"
        leftIcon={<Icon name="user" size={24} color="#00497C" />}
        value={document}
      />
      <Input
        secureTextEntry={true}
        onChangeText={(password) => setPassword(password)}
        inputStyle={css.input_login}
        inputContainerStyle={css.input_container_password}
        title="button title"
        placeholder="Password"
        leftIcon={<Icon name="lock" size={24} color="#00497C" />}
        value={password}
      />
      <Button
        onPress={() => login(document, password, navigation)}
        buttonStyle={css.btn_login}
        title="Log In"
        type="solid"
      />
    </SafeAreaView>
  );
}

function login(d, p, n) {
  const document = d;
  const password = p;
  const datasend = {
    method: "POST",
    body: JSON.stringify({
      document: document,
      password: password,
    }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };
  fetch("http://192.168.1.7:3050/users/login", datasend)
    .then((res) => res.json())
    .then((data) => {
      //console.log(data);
      //console.log(typeof data[0]);
      if (data[0] !== undefined) {
        console.log("CREDENCIALES INCORRECTAS");
        ToastAndroid.show("Documento o Password Incorrecto", ToastAndroid.LONG);
      } else {
        if (data["user_type"] === 1 || data["user_type"] === 2) {
          Auth.default._storeData(data["document"])
          ToastAndroid.show("Bienvenido!! " + data["username"],ToastAndroid.LONG);
          n.navigate("Root", { screen: "Home" });
        } else {
          ToastAndroid.show("Usuario Bloqueado", ToastAndroid.LONG);
        }
      }
    })
    .catch(console.log);
}
